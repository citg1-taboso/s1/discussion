// directory that contains files used to build an application.
// reverse domain name notation
package com.zuitt;

import java.util.Locale;

// entry point of a java program and is responsible for executing our code.
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        int myNum;

        int myNum2 = 30;
        System.out.println("Result of variable:");
        System.out.println(myNum2);

        //Contants
        final int PRINCIPAL = 1000;
        // you cannot change the initial value of the variable

        // Data Types
        // Primitive - single values/simple values
        // Non primitive - multiple values ex. String, null values

        // Primitive
        char letter = 'A';
        char letter1 = 'B';
        char letter2 = 'C';
        char letter3 = 'D';
        char letter4 = 'E';

        // conditional statements
        boolean isBlack = true;

        // -128 to 127
        byte students = 127;

        // -32768 to 32767
        short seats = 32767;

        // using _ for readability chuya na ani na feature i did not know this
        int localPopulation = 2_147_483_647;

        // long integer
        long worldPopulation = 7_862_081_145L;

        // FLOAT and DOUBLE
        // there difference is the number of decimal places.
        float price = 12.99F;
        double temperature = 15869.8623941;

        System.out.println("Result of getClass Method:");
        System.out.println(((Object)temperature).getClass());

        // [NON PRIMITIVE DATA TYPES]
        String name = "John Doe";
        System.out.println("Result of Non-primitive:");
        System.out.println(name);

        String editName = name.toLowerCase();
        String uppercaseName = name.toUpperCase();

        System.out.println(editName);
        System.out.println(uppercaseName);

        // [TYPE CASTING]
        // Implicit Casting
        int num1 = 5;
        double num2 = 2.5;
        double total = num1 + num2;
        System.out.println("Implicit casting: " + total);
        //System.out.println(total);

        // Explicit Casting
        int num3 = 5;
        double num4 = 2.7;

        int anotherTotal = num3 + (int)num4;
        System.out.println("Result from Explicit casting: " + anotherTotal);

    }
}
